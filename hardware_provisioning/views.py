# -*- coding: utf-8 -*-

from __future__ import unicode_literals


from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.http import HttpResponseForbidden
from django.core.urlresolvers import reverse
from django.utils import timezone

from .forms import LoanDeclareForm, LoanTransferForm, LoanReturnForm
from .models import Item, Loan


@login_required
def item_list(request):
    items = Item.objects.all().order_by('storage', 'type', 'designation')

    # FIXME: suboptimal
    items = [i for i in items.filter() if i.is_available()]
    return render(request, 'hardware_provisioning/item_list.html', {
        'items': items,
    })


@login_required
def item_borrow(request, pk):
    item = get_object_or_404(Item, pk=pk)

    if not item.is_available():
        return HttpResponseForbidden('Item non disponible')

    if request.method == 'POST':
        form = LoanDeclareForm(request.POST)
        if form.is_valid():
            loan = Loan.objects.create(
                item=item,
                loan_date=timezone.now(),
                loan_date_end=form.cleaned_data['loan_date_end'],
                user=request.user,
            )
            messages.success(
                request, "Emprunt de {} ({}) enregistré".format(
                    item.designation, item.type))
            if not loan.loan_date_end:
                messages.warning(
                    request,
                    "N'oubliez pas de notifier le retour de l'objet le temps venu")
            return redirect(reverse('hardware_provisioning:loan-list'))
    else:
        form = LoanDeclareForm()

    return render(request, 'hardware_provisioning/item_borrow.html', {
        'item': item,
        'form': form,
    })


@login_required
def loan_return(request, pk):
    loan = get_object_or_404(Loan, pk=pk)

    if not loan.user_can_close(request.user):
        return HttpResponseForbidden('Non autorisé')

    if request.method == 'POST':
        form = LoanReturnForm(request.POST)
        if form.is_valid():
            messages.success(
                request,
                'Le matériel {} a été marqué comme rendu'.format(
                    loan.item))
            loan.item.give_back(form.cleaned_data['storage'])
            return redirect(reverse('hardware_provisioning:loan-list'))
    else:
        form = LoanReturnForm()

    return render(request, 'hardware_provisioning/return.html', {
        'loan': loan,
        'form': form,
    })


@login_required
def loan_transfer(request, pk):
    """ Transfer something loaned to another member
    """
    old_loan = get_object_or_404(Loan, pk=pk)

    if not old_loan.user_can_close(request.user):
        return HttpResponseForbidden()

    if request.method == 'POST':
        form = LoanTransferForm(request.POST)
        if form.is_valid():
            old_loan.item.give_back()
            Loan.objects.create(
                user=form.cleaned_data['target_user'],
                loan_date=timezone.now(),
                item=old_loan.item)
            messages.success(
                request,
                "Le matériel {} a été transféré à l'adhérent \"{}\"".format(
                    old_loan.item,
                    form.data['target_user']))
            return redirect(reverse('hardware_provisioning:loan-list'))

    else:
        form = LoanTransferForm()

    return render(request, 'hardware_provisioning/transfer.html', {
        'form': form,
        'loan': old_loan,
    })


@login_required
def loan_list(request):
    view = 'old' if 'old' in request.GET else ''

    if view == 'old':
        loans = request.user.loans.finished().order_by('-loan_date_end')
    else:
        loans = request.user.loans.running()

    return render(request, 'hardware_provisioning/list.html', {
        'loans': loans,
        'view': view,
    })


@login_required
def loan_detail(request, pk):
    return render(request, 'hardware_provisioning/detail.html', {})
