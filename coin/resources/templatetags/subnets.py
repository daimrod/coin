# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import template
from netaddr import IPNetwork


register = template.Library()

@register.filter
def prettify(subnet):
    """Prettify an IPv4 subnet by remove the subnet length when it is equal to /32
    """
    # Support IPSubnet objects, who have a IPNetwork as attribute
    if hasattr(subnet, "inet") and isinstance(subnet.inet, IPNetwork):
        subnet = subnet.inet
    if isinstance(subnet, IPNetwork):
        if subnet.version == 4 and subnet.prefixlen == 32:
            return str(subnet.ip)
        else:
            return str(subnet)
    return subnet
