# -*- coding: utf-8 -*-
from __future__ import unicode_literals

EXTRA_INSTALLED_APPS = (
    'vpn',
    'dsl_ldap',
)

LDAP_ACTIVATE = True

# Instance LDAP de développement:
#   adresse du serveur : ldapdev.illyse.org
#   port ldap: 389
#   port ldaps: 636

ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_NEVER)

DATABASES = {
    # Base de donnée du SI
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'illyse_coin',
        'USER': 'illyse_coin',
        'PASSWORD': '',
        'HOST': '',  # Empty for localhost through domain sockets
        'PORT': '',  # Empty for default
    },
    # LDAP backend pour stockage et mise à jour de données
    'ldap': {
        'ENGINE': 'ldapdb.backends.ldap',
        'NAME': 'ldap://ldapdev.illyse.org:389/',
        'TLS': True,
        'USER': 'cn=illysedev,ou=services,o=ILLYSE,l=Villeurbanne,st=RHA,c=FR',
        'PASSWORD': 'gfj83-E8ECgGh23JK_Ol12'
    }
}

# LDAP Base DNs
LDAP_USER_BASE_DN = "ou=users,ou=unix,o=ILLYSE,l=Villeurbanne,st=RHA,c=FR"
LDAP_GROUP_BASE_DN = "ou=groups,ou=unix,o=ILLYSE,l=Villeurbanne,st=RHA,c=FR"
VPN_CONF_BASE_DN = "ou=vpn,o=ILLYSE,l=Villeurbanne,st=RHA,c=FR"

# First UID to use for users
LDAP_USER_FIRST_UID = 2000

DATABASE_ROUTERS = ['ldapdb.router.Router']

GRAPHITE_SERVER = "http://graphite-dev.illyse.org"

DEFAULT_FROM_EMAIL = "adminsys@illyse.org"

FEEDS = (('isp', 'http://www.illyse.net/feed/', 3),
         ('ffdn', 'http://www.ffdn.org/fr/rss.xml', 3))

MEMBER_MEMBERSHIP_INFO_URL = 'https://www.illyse.org/projects/failocal/wiki/Cotisation'
