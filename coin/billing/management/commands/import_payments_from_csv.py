# -*- coding: utf-8 -*-
"""
Import payments from a CSV file from a bank.  The payments will automatically be
parsed, and there'll be an attempt to automatically match payments with members.

The matching is performed using the label of the payment.
- First, try to find a string such as 'ID-42' where 42 is the member's ID
- Second (if no ID found), try to find a member username (with no ambiguity with
  respect to other usernames)
- Third (if no username found), try to find a member family name (with no
  ambiguity with respect to other family name)

This script will check if a payment has already been registered with same
properies (date, label, price) to avoid creating duplicate payments inside coin.

By default, only a dry-run is perfomed to let you see what will happen ! You
should run this command with --commit if you agree with the dry-run.
"""

from __future__ import unicode_literals

# Standard python libs
import json
import os

# Django specific imports
from argparse import RawTextHelpFormatter
from django.core.management.base import BaseCommand, CommandError

# Coin specific imports
from coin.billing.import_payments_from_csv import process, add_new_payments

################################################################################


class Command(BaseCommand):

    help = __doc__

    def create_parser(self, *args, **kwargs):
        parser = super(Command, self).create_parser(*args, **kwargs)
        parser.formatter_class = RawTextHelpFormatter
        return parser

    def add_arguments(self, parser):

        parser.add_argument(
            'filename',
            type=str,
            help="The CSV filename to be parsed"
        )

        parser.add_argument(
            '--commit',
            action='store_true',
            dest='commit',
            default=False,
            help='Agree with the proposed change and commit them'
        )

    def handle(self, *args, **options):

        assert options["filename"] != ""
        if not os.path.isfile(options["filename"]):
            raise CommandError("This file does not exists.")

        f = open(options["filename"], "r")
        new_payments = process(f)

        number_of_new_payments = len(new_payments)

        if (number_of_new_payments > 0):
            print("======================================================")
            print("   > New payments found")
            print(json.dumps(new_payments, indent=4, separators=(',', ': ')))
        print("======================================================")
        print("Number of new payments found           : " + str(number_of_new_payments))
        print("Number of new payments matched         : " + str(len([p for p in new_payments if     p["member_matched"]])))
        print("Number of payments not matched         : " + str(len([p for p in new_payments if not p["member_matched"]])))
        print("======================================================")

        if number_of_new_payments == 0:
            print("Nothing to do, everything looks up to date !")
            return

        if not options["commit"]:
            print("Please carefully review the matches, then if everything \n"
                  "looks alright, use --commit to register these new payments.")
        else:
            add_new_payments(new_payments)
