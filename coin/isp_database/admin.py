# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.forms import ModelForm

from localflavor.fr.forms import FRPhoneNumberField

from coin.isp_database.models import ISPInfo, RegisteredOffice, OtherWebsite, ChatRoom, CoveredArea, BankInfo

class ISPAdminForm(ModelForm):

    class Meta:
        model = ISPInfo
        exclude = []

    phone_number = FRPhoneNumberField(required=False,
                                      help_text='Main contact phone number')


class SingleInstanceAdminMixin(object):
    """Hides the "Add" button when there is already an instance"""
    def has_add_permission(self, request):
        num_objects = self.model.objects.count()
        if num_objects >= 1:
            return False
        return super(SingleInstanceAdminMixin, self).has_add_permission(request)


class RegisteredOfficeInline(admin.StackedInline):
    model = RegisteredOffice
    extra = 0
    fieldsets = (
        ('', {'fields': (
             ('street_address', 'extended_address', 'post_office_box'),
             ('postal_code', 'locality'),
             ('region', 'country_name'))}),
        ('Extras', {
             'fields': ('siret',),
             'description': 'Ces champs ne font pas partie de la spécification db.ffdn.org mais sont utilisés sur le site'})
        )



class OtherWebsiteInline(admin.StackedInline):
    model = OtherWebsite
    extra = 0


class ChatRoomInline(admin.StackedInline):
    model = ChatRoom
    extra = 0


class CoveredAreaInline(admin.StackedInline):
    model = CoveredArea
    extra = 0


class BankInfoInline(admin.StackedInline):
    model = BankInfo
    extra = 0

    fieldsets = (('', {
                'fields': ('iban', 'bic', 'bank_name', 'check_order'),
                'description': (
                    'Les coordonnées bancaires ne font pas partie de la'+
                    ' spécification db.ffdn.org mais sont utilisées par le'+
                    ' site (facturation notamment).')
    }),)


class ISPInfoAdmin(SingleInstanceAdminMixin, admin.ModelAdmin):
    model = ISPInfo
    fieldsets = (
        ('General', {'fields': (
            ('name', 'shortname'),
            'description',
            'logoURL',
            ('creationDate', 'ffdnMemberSince'),
            'progressStatus',
            ('latitude', 'longitude'))}),
        ('Contact', {'fields': (
            ('email', 'mainMailingList'),
            'website', 'phone_number')}),
        ('Extras', {
            'fields': ('administrative_email', 'support_email', 'lists_url'),
            'description':
                'Ces champs ne font pas partie de la spécification db.ffdn.org mais sont utilisés sur le site'
        }),
    )

    inlines = (RegisteredOfficeInline, BankInfoInline, OtherWebsiteInline, ChatRoomInline,
               CoveredAreaInline)
    save_on_top = True

    # Use custom form
    form = ISPAdminForm

admin.site.register(ISPInfo, ISPInfoAdmin)
