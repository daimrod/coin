# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('maillists', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='maillinglistsubscription',
            options={'verbose_name': 'abonnement \xe0 une liste mail', 'verbose_name_plural': 'abonnements aux listes mail'},
        ),
        migrations.AlterField(
            model_name='maillinglist',
            name='email',
            field=models.EmailField(unique=True, max_length=254, verbose_name="adresse mail d'envoi"),
        ),
        migrations.AlterField(
            model_name='maillinglist',
            name='short_name',
            field=models.CharField(help_text='c\'est l\'identifiant qui servira \xe0 communiquer avec le serveur de liste mail (typiquement, la partie avant le "@" dans l\'adress )', unique=True, max_length=50, verbose_name='identifiant technique'),
        ),
        migrations.AlterField(
            model_name='maillinglistsubscription',
            name='maillinglist',
            field=models.ForeignKey(verbose_name='liste mail', to='maillists.MaillingList'),
        ),
        migrations.AlterField(
            model_name='maillinglistsubscription',
            name='member',
            field=models.ForeignKey(verbose_name='membre', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterUniqueTogether(
            name='maillinglistsubscription',
            unique_together=set([('member', 'maillinglist')]),
        ),
    ]
