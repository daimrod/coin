- hosts: si-demo-server
  sudo: yes
  vars:
    # public host name users will use to access Coin
    public_fqdn: "coin.example.org"
    # directory where configuration templates are stored
    custom_coin_files_directory: "coin-customizations"
    # unix user who will run app server
    user_name: "coin"
    # PostgreSQL database name
    db_name: "illyse_coin"
    # PostgreSQL user name
    db_user: "illyse_coin"
    # PostgreSQL password
    db_password: "illyse_coin_change_me"
    # PostgreSQL address
    db_host: "localhost"
    # PostgreSQL port
    db_port: "5432"
    # Gunicorn server binding address (address on which the process will listen)
    gunicorn_binding_address: "127.0.0.1"
    # Gunicorn server listening port
    gunicorn_port: "3036"
    # Path of the directory where statics assets will be stored (and served by web server)
    www_static_assets_directory: "/var/www/coin/static"
    # Enable or disable configuration of lighttpd as reverse proxy
    lighttpd_enabled: "true"
    user_home: "/home/{{user_name}}"
    user_logs_dir: "{{ user_home }}/logs"
    working_directory: "{{user_home}}/coin"
    virtualenv_directory: "{{user_home}}/venv"
    supervisor_tasks_conf_directory: "/etc/supervisor/conf.d/"
    lighttpd_log_file: "/var/log/lighttpd/si-coin-django.log"
    www_user: "www-data"
  tasks:
    # Setup: prerequisites
    # note: we put postgresql as needed, but Coin can work with other backends supported by Coin.
    # as an example, SQLite is known to work
    - name: prerequisites are installed
      apt: pkg=git-core,python-dev,python-pip,libldap2-dev,libpq-dev,libsasl2-dev,python-virtualenv,postgresql,postgresql-server-dev-9.1,python-psycopg2,supervisor
           state=installed

    # Setup: PostgreSQL
    - name: create postgres user "{{ db_user }}"
      postgresql_user: name={{ db_user }} password={{ db_password }} 
      sudo_user: postgres
    - name: create postgres db "{{ db_name }}"
      postgresql_db: name={{ db_name }} encoding="UTF-8" lc_collate='fr_FR.UTF-8' lc_ctype='fr_FR.UTF-8' template=template0 
      sudo_user: postgres
    - name: local socket authentication via password is allowed
      lineinfile: dest=/etc/postgresql/9.1/main/pg_hba.conf line="local   {{ db_name }}     {{ db_user }}                             password"
                  insertafter="^local\s+all\s+postgres\s+peer"
      notify: restart_postgres
    - meta: flush_handlers
    - name: grant all to {{ db_user }} on {{ db_name }}
      postgresql_privs: db={{ db_name }} privs=ALL type=database role={{ db_user }}
      sudo_user: postgres
    # Setup: Coin
    - name: create user {{ user_name }}
      user: name={{ user_name }} state=present shell=/bin/false createhome=yes
    - name: git, get Coin code
      git: repo=git://git.illyse.org/coin.git dest={{ working_directory }} version=HEAD accept_hostkey=yes
      sudo_user: "{{ user_name }}"
    - name: install Coin python requirements
      pip: requirements={{ working_directory }}/requirements.txt virtualenv={{ virtualenv_directory }}
      sudo_user: "{{ user_name }}"
    - name: copy custom Coin settings file
      template: src={{ custom_coin_files_directory }}/django_local_settings.py.j2 dest={{ working_directory }}/coin/settings_local.py
                owner={{ user_name }} group={{ user_name }}
    - name: django migrations are applied
      command: "{{ virtualenv_directory }}/bin/python {{ working_directory }}/manage.py migrate"
      sudo_user: "{{ user_name }}"
    - name: gunicorn is installed
      pip: name=gunicorn virtualenv={{ virtualenv_directory }}
      sudo_user: "{{ user_name }}"
    - name: log directory is present in user's home
      file: path={{ user_logs_dir }} state=directory owner={{ user_name }} group={{ user_name }}
    - name: wsgi.py is present
      template: src={{ custom_coin_files_directory }}/wsgi.py.j2 dest={{ working_directory }}/wsgi.py
                owner={{ user_name }} group={{ user_name }}
    - name: supervisord config file is present
      template: src={{ custom_coin_files_directory }}/supervisor-coin.conf.j2 dest={{ supervisor_tasks_conf_directory }}/supervisor-coin.conf
      notify:
        - reread_supervisor_config
        - restart_coin_supervisor_task
    - name: static assets directory exists
      file: path={{ www_static_assets_directory }} state=directory
            owner={{ www_user }} group={{ user_name }} mode=0774
    - name: django collect static assets files
      command: "{{ virtualenv_directory }}/bin/python {{ working_directory }}/manage.py collectstatic --noinput"
      sudo_user: "{{ user_name }}"

    # Configure lighttpd as reverse proxy (only if lighttpd_enabled var is "true")
    - name: lighttpd is installed
      apt: pkg=lighttpd state=installed
      when: lighttpd_enabled == "true"
    - name: lighttpd mod proxy is enabled
      file: src=/etc/lighttpd/conf-available/10-proxy.conf
            dest=/etc/lighttpd/conf-enabled/10-proxy.conf
            state=link
      notify: restart_lighttpd
      when: lighttpd_enabled == "true"
    - name: lighttpd mod accesslog is enabled
      file: src=/etc/lighttpd/conf-available/10-accesslog.conf
            dest=/etc/lighttpd/conf-enabled/10-accesslog.conf
            state=link
      notify: restart_lighttpd
      when: lighttpd_enabled == "true"
    - name: lighttpd config file for {{ public_fqdn }} is present
      template: src={{ custom_coin_files_directory }}/lighttpd-coin.conf.j2 dest=/etc/lighttpd/conf-available/20-coin.conf
      notify: restart_lighttpd
      when: lighttpd_enabled == "true"
    - name: lighttpd config file for {{ public_fqdn }} is enabled
      file: src=/etc/lighttpd/conf-available/20-coin.conf
            dest=/etc/lighttpd/conf-enabled/20-coin.conf
            state=link
      notify: restart_lighttpd
      when: lighttpd_enabled == "true"
  handlers:
    - name: restart_postgres
      service: name=postgresql state=reloaded
    - name: reread_supervisor_config
      supervisorctl: name=coin-si-gunicorn state=present
    - name: restart_coin_supervisor_task
      supervisorctl: name=coin-si-gunicorn state=restarted
    - name: restart_lighttpd
      service: name=lighttpd state=restarted
