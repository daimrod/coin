# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig
import coin.apps

from . import urls


class HousingConfig(AppConfig, coin.apps.AppURLs):
    name = 'housing'
    verbose_name = "Gestion d'accès Housing"

    exported_urlpatterns = [('housing', urls.urlpatterns)]
