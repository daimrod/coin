# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from polymorphic.admin import PolymorphicChildModelAdmin

from coin.configuration.admin import ConfigurationAdminFormMixin
from simple_dsl.models import SimpleDSL


class SimpleDSLInline(admin.StackedInline):
    model = SimpleDSL


class SimpleDSLAdmin(ConfigurationAdminFormMixin, PolymorphicChildModelAdmin):
    base_model = SimpleDSL
    # Used for form inclusion (when browsing a subscription object in the
    # admin, SimpleDSLInline will be displayed)
    inline = SimpleDSLInline
    # Since we don't provide a view, don't display a "view on site" link
    # in the admin.
    view_on_site = False

admin.site.register(SimpleDSL, SimpleDSLAdmin)
